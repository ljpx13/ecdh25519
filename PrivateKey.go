package ecdh25519

import (
	"errors"
	"io"

	"golang.org/x/crypto/curve25519"
)

// PrivateKey is an alias for a byte array of size 32.
type PrivateKey [PrivateKeySize]byte

// PrivateKeySize is the size of a PrivateKey, in bytes.
const PrivateKeySize = 32

// SharedSecretSize is the size of a produced shared secret.
const SharedSecretSize = 32

// ErrInvalidPrivateKeyLength is returned when PrivateKeyFromBytes is called
// with an invalid number of bytes.
var ErrInvalidPrivateKeyLength = errors.New("the provided private key was not 32 bytes in length")

// GenerateKey generates a new PrivateKey using the provided source of random
// bytes.
func GenerateKey(rand io.Reader) (*PrivateKey, error) {
	var r PrivateKey
	if _, err := rand.Read(r[:]); err != nil {
		return nil, err
	}

	r[0] &= 248
	r[31] &= 127
	r[31] |= 64

	return &r, nil
}

// PrivateKeyFromBytes takes the provided input bytes and uses them as a
// PrivateKey.
func PrivateKeyFromBytes(buf []byte) (*PrivateKey, error) {
	if len(buf) != PrivateKeySize {
		return nil, ErrInvalidPrivateKeyLength
	}

	var rx [PrivateKeySize]byte
	n := copy(rx[:], buf)
	if n != PrivateKeySize {
		return nil, ErrInvalidPrivateKeyLength
	}

	r := PrivateKey(rx)
	return &r, nil
}

// ToBytes returns the PrivateKey as a normal byte slice.
func (k *PrivateKey) ToBytes() []byte {
	r := make([]byte, PrivateKeySize)
	kx := [PrivateKeySize]byte(*k)
	copy(r, kx[:])
	return r
}

// PublicKey derives the PublicKey from the PrivateKey.
func (k *PrivateKey) PublicKey() *PublicKey {
	var rx [PublicKeySize]byte
	kx := [PrivateKeySize]byte(*k)

	curve25519.ScalarBaseMult(&rx, &kx)
	r := PublicKey(rx)
	return &r
}

// SharedSecretWith computes the shared secret for the PrivateKey and the
// provided PublicKey.
func (k *PrivateKey) SharedSecretWith(x *PublicKey) []byte {
	var r [SharedSecretSize]byte
	kx := [PrivateKeySize]byte(*k)
	xx := [PublicKeySize]byte(*x)

	curve25519.ScalarMult(&r, &kx, &xx)
	return r[:]
}
