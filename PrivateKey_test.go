package ecdh25519

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestPrivateToAndFromBytes(t *testing.T) {
	// Arrange.
	prv1, err := GenerateKey(rand.Reader)
	test.That(t, err, is.Nil())

	raw := prv1.ToBytes()

	// Act.
	prv2, err := PrivateKeyFromBytes(raw)
	test.That(t, err, is.Nil())

	// Assert.
	b1 := [PrivateKeySize]byte(*prv1)
	b2 := [PrivateKeySize]byte(*prv2)
	test.That(t, b1, is.EqualTo(b2))
}

func TestSharedSecretWith(t *testing.T) {
	// Arrange.
	prv1, err := GenerateKey(rand.Reader)
	test.That(t, err, is.Nil())

	prv2, err := GenerateKey(rand.Reader)
	test.That(t, err, is.Nil())

	pub1 := prv1.PublicKey()
	pub2 := prv2.PublicKey()

	// Act.
	ss1 := prv1.SharedSecretWith(pub2)
	ss2 := prv2.SharedSecretWith(pub1)

	// Assert.
	test.That(t, bytes.Compare(ss1, ss2), is.EqualTo(0))

	sum := 0
	for _, b := range ss1 {
		sum += int(b)
	}

	test.That(t, sum, is.NotEqualTo(0))
}

func TestRFC7748Vector(t *testing.T) {
	// Arrange.
	pk1buf, err := hex.DecodeString("77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a")
	test.That(t, err, is.Nil())

	pk2buf, err := hex.DecodeString("5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb")
	test.That(t, err, is.Nil())

	pk1, err := PrivateKeyFromBytes(pk1buf)
	test.That(t, err, is.Nil())

	pk2, err := PrivateKeyFromBytes(pk2buf)
	test.That(t, err, is.Nil())

	pub1 := pk1.PublicKey()
	pub2 := pk2.PublicKey()
	pub1buf := pub1.ToBytes()
	pub2buf := pub2.ToBytes()
	pub1hex := hex.EncodeToString(pub1buf)
	pub2hex := hex.EncodeToString(pub2buf)

	// Act.
	ss1 := pk1.SharedSecretWith(pub2)
	ss2 := pk2.SharedSecretWith(pub1)
	ss1hex := hex.EncodeToString(ss1)
	ss2hex := hex.EncodeToString(ss2)

	// Assert.
	test.That(t, pub1hex, is.EqualTo("8520f0098930a754748b7ddcb43ef75a0dbf3a0d26381af4eba4a98eaa9b4e6a"))
	test.That(t, pub2hex, is.EqualTo("de9edb7d7b7dc1b4d35b61c2ece435373f8343c85b78674dadfc7e146f882b4f"))
	test.That(t, ss1hex, is.EqualTo("4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742"))
	test.That(t, ss2hex, is.EqualTo("4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742"))
}
