package ecdh25519

import "errors"

// PublicKey is an alias for a byte array of size 32.
type PublicKey [PublicKeySize]byte

// PublicKeySize is the size of a PublicKey, in bytes.
const PublicKeySize = 32

// ErrInvalidPublicKeyLength is returned when PublicKeyFromBytes is called
// with an invalid number of bytes.
var ErrInvalidPublicKeyLength = errors.New("the provided public key was not 32 bytes in length")

// PublicKeyFromBytes takes the provided input bytes and uses them as a
// PublicKey.
func PublicKeyFromBytes(buf []byte) (*PublicKey, error) {
	if len(buf) != PublicKeySize {
		return nil, ErrInvalidPublicKeyLength
	}

	var rx [PublicKeySize]byte
	n := copy(rx[:], buf)
	if n != PublicKeySize {
		return nil, ErrInvalidPublicKeyLength
	}

	r := PublicKey(rx)
	return &r, nil
}

// ToBytes returns the PublicKey as a normal byte slice.
func (k *PublicKey) ToBytes() []byte {
	r := make([]byte, PublicKeySize)
	kx := [PublicKeySize]byte(*k)
	copy(r, kx[:])
	return r
}
