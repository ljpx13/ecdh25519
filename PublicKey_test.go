package ecdh25519

import (
	"crypto/rand"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestPublicToAndFromBytes(t *testing.T) {
	// Arrange.
	prv, err := GenerateKey(rand.Reader)
	test.That(t, err, is.Nil())

	pub1 := prv.PublicKey()
	raw := pub1.ToBytes()

	// Act.
	pub2, err := PublicKeyFromBytes(raw)
	test.That(t, err, is.Nil())

	// Assert.
	b1 := [PublicKeySize]byte(*pub1)
	b2 := [PublicKeySize]byte(*pub2)
	test.That(t, b1, is.EqualTo(b2))
}
